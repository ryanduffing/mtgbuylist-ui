module.exports = function (grunt) {
    'use strict';

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        autoprefixer: {
            app: {
                files: {
                    './temp/assets/css/styles.css': './temp/assets/css/styles.css'
                }
            }
        },

        clean: {
            afterbuild: './temp',
            prebuild: ['./dist/*', './temp/*'],
            templates: './src/app/templates.js'
        },

        copy: {
            app: {
                src: './src/index.html',
                dest: './temp/index.html'
            },
            prod: {
                files: [
                    {expand: true, cwd: './temp/', src: '**/*', dest: './dist/'},
                    {expand: true, cwd: './src/', src: ['404.html', 'robots.txt'], dest: './dist/'},
                    {expand: true, cwd: './src/assets/', src: '**', dest: './dist/assets'},
                    {expand: true, cwd: './src/bower_components/font-awesome/fonts/', src: '*.*', dest: './dist/assets/fonts'},
                    {expand: true, cwd: './src/bower_components/bootstrap-sass/assets/fonts/bootstrap', src: '*.*', dest: './dist/assets/fonts'}
                ]
            }
        },

        coverage: {
            default: {
                options: {
                    thresholds: {
                        'statements': 80,
                        'branches': 80,
                        'lines': 80,
                        'functions': 80
                    },
                    dir: 'coverage',
                    root: ''
                }
            }
        },

        eol: {
            dist: {
                options: {
                    eol: 'lf',
                    replace: true
                },
                files: [{
                    src: ['./temp/index.html']
                }]
            }
        },

        express: {
            options: {
                port: 9000,
                hostname: '0.0.0.0',
                bases: ['temp', 'src'],
                livereload: true,
                open: true
            },
            app: {
                options: {
                    server: './server/server.js'
                }
            },
            prod: {
                options: {
                    port: 9001,
                    bases: 'dist',
                    server: './server/server.js',
                    livereload: false
                }
            }
        },

        filerev: {
            scripts: {
                src:[
                    './temp/**/*.js'
                ]
            },
            styles: {
                src: [
                    './temp/**/*.css'
                ]
            }
        },

        html2js: {
            app: {
                options: {
                    base: './src',
                    existingModule: true,
                    singleModule: true,
                    htmlmin: {
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        removeAttributeQuotes: true,
                        removeComments: true,
                        removeEmptyAttributes: true,
                        removeRedundantAttributes: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true
                    }
                },
                src: [
                    'src/app/**/*.html'
                ],
                dest: './src/app/templates.js',
                module: 'app'
            }
        },

        imagemin: {
            app: {
                files: [{
                    expand: true,
                    cwd: './dist/assets/images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: './dist/assets/images/'
                }]
            }
        },

        jshint: {
            options: {
                reporter: require('jshint-stylish')
            },
            all: [
                './src/app/**/*.js',
                './test/**/*.js'
            ]
        },

        karma: {
            unit: {
                configFile: './karma.conf.js',
                singleRun: true
            }
        },

        ngsrc: {
            target: {
                cwd: 'src/',
                src: ['app/**/*.js'],
                dest: ['./temp/index.html']
            }
        },

        sass: {
            options: {
                noCache: true,
                sourcemap: 'none',
                style: 'compressed'
            },
            app: {
                files: {
                    './temp/assets/css/styles.css': './src/app/startup/base.scss'
                }
            }
        },

        scsslint: {
            allFiles: [
                './src/app/**/*.scss'
            ],
            options: {
                bundleExec: false
            }
        },

        usemin: {
            html: ['./temp/index.html']
        },

        useminPrepare: {
            options: {
                dest: './temp',
                root: './src'
            },
            html: './temp/index.html'
        },

        watch: {
            options: {
                livereload: true,
                interval: 5007
            },
            css: {
                files: [
                    './temp/assets/**/*.css'
                ]
            },
            html: {
                files: [
                    './src/**/*.html'
                ],
                tasks: [
                    'copy:app',
                    'wiredep',
                    'ngsrc'
                ]
            },
            js: {
                files: [
                    './src/app/**/*.js',
                    './src/app/*.js'
                ]
            },
            sass: {
                files: [
                    './src/**/*.scss'
                ],
                tasks: [
                    'sass:app',
                    'autoprefixer:app'
                ],
                options: {
                    livereload: false
                }
            }
        },

        wiredep: {
            app: {
                src: [
                    './src/app/startup/base.scss',
                    './temp/index.html'
                ],
                ignorePath: '../src/',
                exclude: [
                    '/jquery/',
                    'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
                    'bower_components/angular-mocks/angular-mocks.js',
                    'bower_components/jasmine-jquery/lib/jasmine-jquery.js'
                ]
            }
        }
    });

    grunt.registerTask('default', []);

    grunt.registerTask('build', [
        'clean:prebuild',
        'copy:app',
        'sass:app',
        'autoprefixer:app',
        'html2js',
        'ngsrc',
        'wiredep',
        'eol',
        'useminPrepare',
        'concat:generated',
        'cssmin:generated',
        'uglify:generated',
        'filerev',
        'usemin',
        'copy:prod',
        'imagemin:app',
        'clean:afterbuild',
        'clean:templates'
    ]);

    grunt.registerTask('localbuild', [
        'clean:prebuild',
        'copy:app',
        'wiredep',
        'sass:app',
        'autoprefixer:app',
        'ngsrc'
    ]);

    grunt.registerTask('server', ['localbuild', 'express:app', 'watch']);
    grunt.registerTask('server:dev', ['server']);
    grunt.registerTask('server:prod', ['build', 'express:prod', 'express-keepalive']);

    grunt.registerTask('test', ['jshint', 'scsslint', 'karma:unit', 'coverage']);

    grunt.registerTask('sasslint', ['scsslint']);
};
