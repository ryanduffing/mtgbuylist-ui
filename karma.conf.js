// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function(config) {
    'use strict';
    config.set({
        // base path, that will be used to resolve files and exclude
        basePath: '',

        // testing framework to use (jasmine/mocha/qunit/...)
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: [
            'node_modules/phantomjs-polyfill/bind-polyfill.js',
            'src/bower_components/jquery/dist/jquery.js',
            'src/bower_components/angular/angular.js',
            'src/bower_components/ngstorage/ngStorage.min.js',
            'src/bower_components/angular-ui-router/release/angular-ui-router.min.js',
            'src/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
            'src/bower_components/fastclick/lib/fastclick.js',
            'src/bower_components/underscore/underscore-min.js',
            'src/bower_components/angular-mocks/angular-mocks.js',                // Only required in testing, not in app
            'src/app/app.module.js',
            'src/app/**/*.js',            // Load child controllers third (after modules exist),
            'test/**/*.js'
        ],

        proxies: {
            '/assets/image': '/base/src/assets/image'
        },

        preprocessors: {
        },

        ngHtml2JsPreprocessor: {
            // strip src from the file path
            stripPrefix: 'src/'
        },

        // list of files / patterns to exclude
        exclude: [],

        // web server port
        port: 9876,

        // set timeout to 30s
        browserNoActivityTimeout: 30000,

        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,

        reporters: ['progress', 'coverage', 'xml', 'junit'],

        coverageReporter: {
            reporters: [
                { type: 'html', dir: 'coverage' },
                { type: 'text-summary', dir: 'coverage' },
                { type: 'json', dir: 'coverage' }
            ]
        },

        junitReporter: {
            outputFile: 'test-results.xml',
            suite: ''
        },

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS'],

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false,

        // Plugins
        plugins: [
            'karma-coverage',
            'karma-jasmine',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-xml-reporter',
            'karma-phantomjs-launcher',
            'karma-ng-html2js-preprocessor',
            'karma-junit-reporter'
        ]
    });
};
