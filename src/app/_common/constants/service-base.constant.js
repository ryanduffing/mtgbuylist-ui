(function () {
    'use strict';

    angular.module('app')
        .constant('serviceBase', getServiceBase());

    function getServiceBase() {
        var servicePath = '/app/mtgbuylist-api/';
        var host = window.location.host;

        return 'https://' + host + servicePath;
    }
})();
