(function(){
    'use strict';

    angular.module('card-results', [])
        .directive('cardResults', cardResultsDirective);

    cardResultsDirective.$inject = [];

    function cardResultsDirective() {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                cardQuery: '=',
                cardResults: '=',
                cartQuantity: '=',
                clearOnAdd: '=',
                focusInput: '='
            },
            controller: CardResultsController,
            controllerAs: 'vm',
            templateUrl: 'app/_common/directives/partials/card-results.html',
            bindToController: true
        };
    }

    CardResultsController.$inject = ['cartService'];

    function CardResultsController(cartService) {
        var vm = this;

        // scope methods
        vm.addCardToCart = addCardToCart;
        vm.addToResultList = addToResultList;
        vm.getBackgroundImage = getBackgroundImage;
        vm.setViewMode = setViewMode;

        // scope variables
        vm.viewMode = 'card';

        /////////////////////////////////////////////////////////////////

        function addCardToCart(card) {
            if(card.quantity) {
                card.quantity = parseInt(card.quantity);
                vm.cartQuantity += card.quantity;

                if (card.quantity > 0) {
                    var localCard = {};

                    angular.copy(card, localCard);

                    cartService.addToCart(localCard);
                }
                card.quantity = 0;

                vm.focusInput = true;
                vm.cardQuery = '';

                if (vm.clearOnAdd) {
                    vm.cardResults = [];
                }
            }
        }

        function addToResultList() {
            vm.limit += 12;
        }

        function getBackgroundImage(card) {
            var backgroundImageObject;

            if(!card.ImageUrl) {
                card.ImageUrl = '//image.deckbrew.com/mtg/multiverseid/0.jpg';
            }

            if(card.IsFoil) {
                backgroundImageObject = {
                    'background-image': 'url("/assets/images/download1.png"), url("' + card.ImageUrl + '")'
                }
            }
            else {
                backgroundImageObject = {
                    'background-image': 'url("' + card.ImageUrl + '")'
                }
            }

            return backgroundImageObject;
        }

        function setViewMode(mode) {
            vm.viewMode = mode;
        }
    }
})();
