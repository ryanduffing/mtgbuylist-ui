(function () {
    'use strict';

    angular.module('directives', [
        'focus-me',
        'card-results'
    ]);
})();
