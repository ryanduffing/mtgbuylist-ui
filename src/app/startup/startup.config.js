(function() {
    'use strict';

    angular.module('app').config(startupConfig);

    startupConfig.$inject = ['$httpProvider', '$urlRouterProvider', 'serviceBase'];

    function startupConfig($httpProvider, $urlRouterProvider, serviceBase) {
        $httpProvider.interceptors.push(['$q', function ($q) {
            return {
                'request': function(config) {
                    var KARMA_TESTING_PORT = '9876';

                    var isApiCall = config.url.indexOf('/api/') === 0;
                    var isKarmaRunner = location.port === KARMA_TESTING_PORT;
                    var isLocalHost = window.location.host.search(/localhost/) > -1;

                    if(!isKarmaRunner && isApiCall && !isLocalHost) {
                        config.url = serviceBase + config.url;
                    }

                    return config || $q.when(config);
                },
                'response': function(response) {
                    return response;
                },
                'responseError': function(response) {
                    return $q.reject(response);
                }
            };
        }]);

        $urlRouterProvider
            .when('/', '/search')
            .when('', '/search')
            .otherwise('/search');
    }
})();
