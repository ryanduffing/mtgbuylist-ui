(function() {
    'use strict';

    angular.module('app').run(runApp);

    runApp.$inject = ['$rootScope'];

    function runApp($rootScope) {

        /////////////////////////////////////////////////////////////////

        init();

        function createApp() {
            /**
             * Listen for route changes.
             */
            $rootScope.$on('$stateChangeStart', function (event, toState) {

            });

            /**
             * Listen for error on route change and stop view loading
             */
            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {

            });

            /**
             * Listen for success on route change and set up new section
             */
            $rootScope.$on('$stateChangeSuccess', function(event, toState) {
                var darkStates = ['search', 'browse'];

                $rootScope.darkBackground = darkStates.indexOf(toState.name) > -1;
            });

            FastClick.attach(document.body);
        }

        function init() {
            createApp();
        }
    }
})();
