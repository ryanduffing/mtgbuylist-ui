(function() {
    'use strict';

    angular.module('app', [
        'ngFileSaver',
        'infinite-scroll',
        'ui.router',
        'ui.bootstrap',
        'ngStorage',
        'browse',
        'cart',
        'common',
        'review',
        'search'
    ]);
})();
