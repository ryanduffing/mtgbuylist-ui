(function() {
    'use strict';

    angular.module('review')
        .config(reviewRoutes);

    reviewRoutes.$inject = ['$stateProvider'];

    function reviewRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('review', {
                url: '/review',
                templateUrl: 'app/review/views/review.html',
                controller: 'ReviewController',
                controllerAs: 'vm',
                resolve: {
                    sellList: ['cartService', function(cartService) {
                        return cartService.getCart();
                    }]
                }
            });
    }
})();
