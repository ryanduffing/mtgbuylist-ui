(function() {
    'use strict';

    angular.module('review.controller', [])
        .controller('ReviewController', ReviewController);

    ReviewController.$inject = ['sellList', 'reviewService'];

    function ReviewController(sellList, reviewService) {
        var vm = this;

        // scope methods
        vm.sortColor = sortColor;
        vm.sortName = sortName;

        // scope variables
        vm.cardList = [];
        vm.runningTotal = 0;

        /////////////////////////////////////////////////////////////////

        init();

        function init() {
            var setNames = _.uniq(_.map(sellList, 'SetName'));
            setNames.sort();

            _.each(setNames, function(set) {
                var setCards = _.filter(sellList, function(sell) {
                    return sell.SetName === set;
                });

                vm.cardList.push({
                    SetName: set,
                    Cards: setCards
                });
            });

            getRunningTotal();
        }

        function getRunningTotal() {
            var localTotal = 0;

            for(var i = 0; i < sellList.length; i++) {
                localTotal += (sellList[i].BuyPrice * sellList[i].quantity);
            }

            vm.runningTotal = localTotal;
        }

        function sortColor(item) {
            return reviewService.sortColor(item);
        }

        function sortName(item) {
            return reviewService.sortName(item);
        }
    }
})();
