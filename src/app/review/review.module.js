(function() {
    'use strict';

    angular.module('review', [
        'review.controller',
        'review.data',
        'review.service'
    ]);
})();