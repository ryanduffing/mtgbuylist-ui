(function() {
    'use strict';

    angular.module('review.service', [])
        .factory('reviewService', reviewService);

    reviewService.$inject = [];

    function reviewService() {

        var service = {
            sortColor: sortColor,
            sortName: sortName
        };

        return service;
        /////////////////////////////////////////////////////////////////

        function sortColor(item) {
            var colorOrder = [
                'colorless',
                'white',
                'blue',
                'black',
                'red',
                'green',
                'gold',
                'artifact',
                'land'
            ];

            return colorOrder.indexOf(item.ColorName);
        }

        function sortName(item) {
            return item.PowerNineName;
        }
    }
})();
