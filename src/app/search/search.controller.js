(function() {
    'use strict';

    angular.module('search.controller', [])
        .controller('SearchController', SearchController);

    SearchController.$inject = ['searchData', 'cartService'];

    function SearchController(searchData, cartService) {
        var vm = this;

        // scope methods
        vm.atCartThreshold = atCartThreshold;
        vm.searchCards = searchCards;

        // scope variables
        vm.cardQuery = '';
        vm.cardResults = [];
        vm.cartQuantity = cartService.getCartQuantity();
        vm.focusInput = false;

        /////////////////////////////////////////////////////////////////

        init();

        function init() {
        }

        function atCartThreshold() {
            return vm.cartQuantity < 100;
        }

        function searchCards(query) {
            vm.limit = 12;

            if(query) {
                searchData.searchCards(query).then(function (results) {
                    vm.cardResults = _.sortBy(results.value, 'PowerNineName');
                });
            }
            else {
                vm.cardResults = [];
            }
        }
    }
})();
