(function () {
    'use strict';

    angular.module('search')
        .config(searchRoutes);

    searchRoutes.$inject = ['$stateProvider'];

    function searchRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('search', {
                url: '/search',
                templateUrl: 'app/search/views/search.html',
                controller: 'SearchController',
                controllerAs: 'vm'
            });
    }
})();
