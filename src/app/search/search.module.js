(function() {
    'use strict';

    angular.module('search', [
        'search.controller',
        'search.data',
        'search.service'
    ]);
})();