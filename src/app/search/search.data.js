(function() {
    'use strict';

    angular.module('search.data', [])
        .factory('searchData', searchData);

    searchData.$inject = ['$http'];

    function searchData($http) {
        var service = {
            getCardById: getCardById,
            searchCards: searchCards,
            searchCardsByNameAndSet: searchCardsByNameAndSet
        };

        return service;

        /////////////////////////////////////////////////////////////////

        function getCardById(cardId) {
            return $http.get('/api/PowerNineCard(' + cardId +')').then(function(response) {
                return response.data;
            });

        }

        function searchCards(name) {
            name = name.replace(/'/g, '\'\'');

            return $http.get('/api/PowerNineCard?$filter=startswith(tolower(PowerNineName),\'' + name.toLowerCase() + '\')&$count=true').then(function(response) {
                return response.data;
            },function(error) {
                return error;
            });
        }

        function searchCardsByNameAndSet(name, setName, isFoil) {
            name = name.replace(/'/g, '\'\'');
            setName = setName.replace(/'/g, '\'\'');

            return $http.get('/api/PowerNineCard?$filter=startswith(tolower(PowerNineName),\'' + name.toLowerCase() + '\') and SetName eq \'' + setName + '\' and IsFoil eq ' + isFoil + '&$count=true').then(function(response) {
                return response.data;
            },function(error) {
                return error;
            });
        }
    }
})();
