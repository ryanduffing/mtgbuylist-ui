(function() {
    'use strict';

    angular.module('export.service', [])
        .factory('exportService', exportService);

    exportService.$inject = ['cartService', 'FileSaver', '$filter', 'reviewService', 'exportConstants'];

    function exportService(cartService, FileSaver, $filter, reviewService, exportConstants) {

        var service = {
            exportExcelBuylist: exportExcelBuylist,
            exportJsonFile: exportJsonFile
        };

        return service;

        /////////////////////////////////////////////////////////////////

        function createStyleFormatters(workbook) {
            var formatters = {};
            var stylesheet = workbook.getStyleSheet();
            var currencyFormat = stylesheet.createNumberFormatter('$#,##0.00');

            for(var key in exportConstants.formatters) {
                if(exportConstants.formatters.hasOwnProperty(key)) {
                    exportConstants.formatters[key].format = currencyFormat.id;

                    formatters[key] = stylesheet.createFormat(exportConstants.formatters[key]);
                }
            }

            formatters['rightAlign'] = stylesheet.createFormat({
                font: {
                    bold: true
                },
                alignment: {
                    horizontal: 'right'
                }
            });

            return formatters;
        }

        function exportExcelBuylist(cardList) {
            var buyList = [];
            var rowIndex = 0;
            var sortedList = extractCardsBySet(cardList);
            var workbook = ExcelBuilder.Builder.createWorkbook();
            var cardSheet = workbook.createWorksheet({name: 'PowerNine Sell List'});
            var formatters = createStyleFormatters(workbook);

            for(var i = 0; i < sortedList.length; i++) {
                // parse cardList data into valid format for worksheet
                buyList.push([sortedList[i].SetName]);
                buyList.push([]);

                cardSheet.setRowInstructions(rowIndex, {
                    height: 30,
                    style: formatters.boldBig.id
                });

                rowIndex += 2;

                buyList.push([
                    'Card Name',
                    'Color',
                    'Quantity',
                    {value: 'Card Price', metadata: {style: formatters.rightAlign.id}},
                    {value: 'Total', metadata: {style: formatters.rightAlign.id}}
                ]);

                cardSheet.setRowInstructions(rowIndex, {
                    style: formatters.boldNormal.id
                });

                rowIndex++;

                for(var j = 0; j < sortedList[i].Cards.length; j++) {
                    var colorName = sortedList[i].Cards[j].ColorName;

                    buyList.push([
                        sortedList[i].Cards[j].PowerNineName + (sortedList[i].Cards[j].IsFoil ? ' - FOIL' : ''),
                        colorName.charAt(0).toUpperCase() + colorName.slice(1),
                        sortedList[i].Cards[j].quantity.toString(),
                        sortedList[i].Cards[j].BuyPrice,
                        sortedList[i].Cards[j].BuyPrice * sortedList[i].Cards[j].quantity
                    ]);

                    cardSheet.setRowInstructions(rowIndex, {
                        style: formatters[colorName].id
                    });

                    rowIndex++;
                }

                // create some empty rows to space out sets.
                buyList.push([]);
                rowIndex++;

                // we don't want two blank rows at the end of the excel document
                if(i !== sortedList.length - 1) {
                    buyList.push([]);
                    rowIndex++;
                }
            }

            // add running total
            buyList.push([
                '',
                '',
                '',
                'Total: ',
                getRunningTotal(cardList)
            ]);

            cardSheet.setRowInstructions(rowIndex, {
                height: 30,
                style: formatters.boldBig.id
            });

            cardSheet.setData(buyList);
            workbook.addWorksheet(cardSheet);

            ExcelBuilder.Builder.createFile(workbook, {type: "blob"}).then(function(data){
                return FileSaver.saveAs(data, 'PowerNine Sell List.xlsx');
            }, function(error) {
                console.log(error);
            });
        }

        function exportJsonFile() {
            var cartJson = JSON.stringify(cartService.getCart());
            var blob = new Blob([cartJson], {type: "text/plain;charset=utf-8"});
            FileSaver.saveAs(blob, 'mtg_export.json');
        }

        function extractCardsBySet(cardList) {
            var sortedList = [];
            var setNames = _.uniq(_.map(cardList, 'SetName'));

            setNames.sort();

            _.each(setNames, function(set) {
                var setCards = _.filter(cardList, function(sell) {
                    return sell.SetName === set;
                });

                setCards = $filter('orderBy')(setCards, [reviewService.sortColor, reviewService.sortName]);

                sortedList.push({
                    SetName: set,
                    Cards: setCards
                });
            });

            return sortedList;
        }

        function getRunningTotal(cardList) {
            var localTotal = 0;

            for(var i = 0; i < cardList.length; i++) {
                localTotal += (cardList[i].BuyPrice * cardList[i].quantity);
            }

            var runningTotal = localTotal;

            return runningTotal;
        }
    }
})();