(function() {
    'use strict';

    angular.module('export', [
        'export.controller',
        'export.service'
    ]);
})();