(function () {
    'use strict';

    angular.module('export')
        .constant('exportConstants', getExportConstants());

    function getExportConstants() {
        var constants = {};

        var formatters = {
            artifact: {
                font: {
                    color: 'FFA52A2A'
                }
            },
            black: {
                font: {
                    color: 'FF000000'
                }
            },
            blue: {
                font: {
                    color: 'FF0000FF'
                }
            },
            boldBig: {
                font: {
                    bold: true,
                    size: 14
                }
            },
            boldNormal: {
                font: {
                    bold: true
                }
            },
            colorless: {
                font: {
                    color: 'FF808080'
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: 'FF000000'
                }
            },
            currency: {
                format: '$#,##0.00'
            },
            green: {
                font: {
                    color: 'FF008000'
                }
            },
            gold: {
                font: {
                    color: 'FFFFFF00'
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: 'FF000000'
                }
            },
            land: {
                font: {
                    color: 'FF000000'
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: 'FF808080'
                }
            },
            red: {
                font: {
                    color: 'FFFF0000'
                }
            },
            white: {
                font: {
                    color: 'FFFFFFFF'
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: 'FF000000'
                }
            }
        };

        constants.formatters = formatters;

        return constants;
    }
})();
