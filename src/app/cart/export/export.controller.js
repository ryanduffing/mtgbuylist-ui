(function() {
    'use strict';

    angular.module('export.controller', [])
        .controller('ExportController', ExportController);

    ExportController.$inject = ['cardList', 'exportService', '$uibModalInstance'];

    function ExportController(cardList, exportService, $uibModalInstance) {
        var vm = this;

        // scope methods
        vm.close = close;
        vm.exportExcelBuylist = exportExcelBuylist;
        vm.exportJsonFile = exportJsonFile;

        // scope variables

        /////////////////////////////////////////////////////////////////

        function close() {
            $uibModalInstance.dismiss('cancel');
        }

        function exportExcelBuylist() {
            exportService.exportExcelBuylist(cardList);
        }

        function exportJsonFile() {
            exportService.exportJsonFile();
        }
    }
})();
