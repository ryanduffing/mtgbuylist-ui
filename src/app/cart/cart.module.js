(function() {
    'use strict';

    angular.module('cart', [
        'cart.controller',
        'cart.data',
        'cart.service',
        'export'
    ]);
})();