(function() {
    'use strict';

    angular.module('cart.service', [])
        .factory('cartService', cartService);

    cartService.$inject = ['$sessionStorage', 'searchData', '$q'];

    function cartService($sessionStorage, searchData, $q) {
        var shoppingCartList;

        var service = {
            addToCart: addToCart,
            clearCart: clearCart,
            getCart: getCart,
            getCartQuantity: getCartQuantity,
            refreshList: refreshList,
            updateCart: updateCart
        };

        return service;

        /////////////////////////////////////////////////////////////////

        function addToCart(card) {
            if(!shoppingCartList) {
                shoppingCartList = [];
            }

            for(var i = 0; i < shoppingCartList.length; i++) {
                if(card.PowerNineRecordId === shoppingCartList[i].PowerNineRecordId) {
                    shoppingCartList[i].quantity += card.quantity;

                    $sessionStorage.shoppingCartList = shoppingCartList;

                    return;
                }
            }

            if(card.quantity > 0) {
                shoppingCartList.push(card);

                $sessionStorage.shoppingCartList = shoppingCartList;
            }
        }

        function clearCart() {
            shoppingCartList = $sessionStorage.shoppingCartList = [];
        }

        function getCart() {
            if(!shoppingCartList) {
                shoppingCartList = $sessionStorage.shoppingCartList;

                if(!shoppingCartList) {
                    shoppingCartList = $sessionStorage.shoppingCartList = [];
                }
            }

            return shoppingCartList;
        }

        function getCartQuantity() {
            var cartList = getCart();
            var quantity = 0;

            _.each(cartList, function(item) {
                quantity += item.quantity;
            });

            return quantity;
        }

        function processRemovalList(removalList, cart) {
            // loop through removalList  and remove relevant items from cart list
            var poppedIndex;

            // we need to loop through the list in reverse in order to preserve the cart's index when
            // removing items
            while((poppedIndex = removalList.pop()) !== undefined) {
                cart.splice(poppedIndex, 1);
            }
        }

        function refreshList() {
            var cartList = getCart();
            var cartLength = cartList.length;
            var cardsProcessed = 0;
            var deferred = $q.defer();
            var errorItems = [];
            var removalIndexes = [];
            var removalItems = [];
            var updatedItems = [];

            // need to loop through EVERY card in the cart, check if it is still for sale, if it is,
            // update the price to be accurate, otherwise add it to a removal list
            _.each(cartList, function(item, index) {
                searchData.searchCardsByNameAndSet(item.PowerNineName, item.SetName, item.IsFoil).then(function(cards) {
                    cardsProcessed++;

                    if(cards.value.length === 0) {
                        removalIndexes.push(index);
                        removalItems.push(item);
                    }
                    else if (cards.value.length === 1) {
                        var tempCard = cards.value[0];

                        if(tempCard.BuyPrice !== item.BuyPrice) {
                            updatedItems.push(item);
                            item.BuyPrice = tempCard.BuyPrice;
                        }
                    }
                    else {
                        errorItems.push(item);
                    }

                    // because of promises we need to manually keep track of how many cards have been processed.
                    if(cardsProcessed === cartLength) {
                        processRemovalList(removalIndexes, cartList);

                        var returnObject = {
                            'Error Items': errorItems,
                            'Removed Items': removalItems,
                            'Updated Items': updatedItems
                        };

                        deferred.resolve(returnObject);
                    }
                });
            });

            return deferred.promise;
        }

        function updateCart(card) {
            if(!shoppingCartList) {
                shoppingCartList = [];
            }

            for(var i = 0; i < shoppingCartList.length; i++) {
                if(shoppingCartList[i].PowerNineRecordId === card.PowerNineRecordId) {
                    if(card.quantity <= 0) {
                        shoppingCartList.splice(i, 1);
                    }
                    else {
                        shoppingCartList[i].quantity = card.quantity;
                    }
                }
            }

            $sessionStorage.shoppingCartList = shoppingCartList;
        }
    }
})();
