(function () {
    'use strict';

    angular.module('cart')
        .config(cartRoutes);

    cartRoutes.$inject = ['$stateProvider'];

    function cartRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('cart', {
                url: '/cart',
                templateUrl: 'app/cart/views/cart.html',
                controller: 'CartController',
                controllerAs: 'vm'
            });
    }
})();
