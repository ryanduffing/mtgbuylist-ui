(function() {
    'use strict';

    angular.module('cart.controller', [])
        .controller('CartController', CartController);

    CartController.$inject = ['cartService', '$uibModal'];

    function CartController(cartService, $uibModal) {
        var vm = this;

        // scope methods
        vm.getRunningTotal = getRunningTotal;
        vm.openExportModal = openExportModal;
        vm.refreshList = refreshList;
        vm.trashCard = trashCard;
        vm.updateCard = updateCard;

        // scope variables
        vm.refreshingCardList = false;
        vm.runningTotal = 0;
        vm.cardList = [];

        /////////////////////////////////////////////////////////////////

        init();

        function getRunningTotal() {
            var localTotal = 0;

            for(var i = 0; i < vm.cardList.length; i++) {
                localTotal += (vm.cardList[i].BuyPrice * vm.cardList[i].quantity);
            }

            vm.runningTotal = localTotal;
        }

        function init() {
            vm.cardList = cartService.getCart();
            getRunningTotal();
        }

        function openExportModal() {
            $uibModal.open({
                templateUrl: 'app/cart/export/views/export.html',
                controller: 'ExportController',
                controllerAs: 'vm',
                size: 'md',
                windowClass: 'export-modal',
                resolve: {
                    cardList: function () {
                        return vm.cardList;
                    }
                }
            });
        }

        function refreshList() {
            vm.refreshingCardList = true;

            cartService.refreshList().then(function (processedCards) {
                getRunningTotal();

                vm.refreshingCardList = false;
            }, function() {
                vm.refreshingCardList = false;
            });
        }

        function trashCard(card) {
            card.quantity = 0;
            cartService.updateCart(card);
        }

        function updateCard(card) {
            if(card.quantity > 0) {
                cartService.updateCart(card);
                getRunningTotal();
            }
            else {
                card.quantity = 1;
            }
        }
    }
})();
