(function() {
    'use strict';

    angular.module('cards.controller', [])
        .controller('CardsController', CardsController);

    CardsController.$inject = ['setName', 'cards', 'cartService'];

    function CardsController(setName, cards, cartService) {
        var vm = this;

        // scope methods
        vm.atCartThreshold = atCartThreshold;

        // scope variables
        vm.setName = setName;
        vm.cardResults = cards;
        vm.cartQuantity = cartService.getCartQuantity();

        /////////////////////////////////////////////////////////////////

        function atCartThreshold() {
            return vm.cartQuantity < 100;
        }
    }
})();
