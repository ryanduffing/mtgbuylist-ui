(function () {
    'use strict';

    angular.module('cards')
        .config(cardsRoutes);

    cardsRoutes.$inject = ['$stateProvider'];

    function cardsRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('browse.cards', {
                url: '/cards/:setId',
                views: {
                    '@': {
                        templateUrl: 'app/browse/cards/views/cards.html',
                        controller: 'CardsController',
                        controllerAs: 'vm',
                        resolve: {
                            setName: ['cardsData', '$stateParams', function(cardsData, $stateParams) {
                                return cardsData.getSet($stateParams.setId).then(function(set) {
                                    return set.Name;
                                });
                            }],
                            cards: ['cardsData', '$stateParams', function(cardsData, $stateParams) {
                                return cardsData.getCardsBySet($stateParams.setId).then(function(cards) {
                                    return cards;
                                });
                            }]
                        }
                    }
                }
            });
    }
})();
