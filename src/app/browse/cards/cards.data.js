(function() {
    'use strict';

    angular.module('cards.data', [])
        .factory('cardsData', cardsData);

    cardsData.$inject = ['$http'];

    function cardsData($http) {
        var service = {
            getCardsBySet: getCardsBySet,
            getSet: getSet
        };

        return service;

        /////////////////////////////////////////////////////////////////

        function getCardsBySet(setId) {
            return $http.get('/api/PowerNineCard?$filter=SetId eq ' + setId + '&$count=true').then(function(response) {
                return response.data.value;
            },function(error) {
                return error;
            });
        }

        function getSet(setId) {
            return $http.get('/api/Set(' + setId + ')').then(function(response) {
                return response.data;
            });
        }
    }
})();
