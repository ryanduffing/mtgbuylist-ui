(function() {
    'use strict';

    angular.module('cards', [
        'cards.controller',
        'cards.data'
    ]);
})();
