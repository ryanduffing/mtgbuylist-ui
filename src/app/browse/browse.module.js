(function() {
    'use strict';

    angular.module('browse', [
        'browse.controller',
        'browse.data',
        'cards'
    ]);
})();
