(function() {
    'use strict';

    angular.module('browse.data', [])
        .factory('browseData', browseData);

    browseData.$inject = ['$http'];

    function browseData($http) {
        var service = {
            getSets: getSets
        };

        return service;

        /////////////////////////////////////////////////////////////////

        function getSets() {
            return $http.get('/api/Set?$filter=ImageUrl ne null').then(function(response) {
                return response.data.value;
            }, function(error) {
                return error;
            });
        }
    }
})();
