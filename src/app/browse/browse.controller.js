(function() {
    'use strict';

    angular.module('browse.controller', [])
        .controller('BrowseController', BrowseController);

    BrowseController.$inject = ['currentSets'];

    function BrowseController(currentSets) {
        var vm = this;

        // scope methods

        // scope variables
        vm.currentSets = currentSets;

        /////////////////////////////////////////////////////////////////
    }
})();
