(function () {
    'use strict';

    angular.module('browse')
        .config(browseRoutes);

    browseRoutes.$inject = ['$stateProvider'];

    function browseRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('browse', {
                url: '/browse',
                templateUrl: 'app/browse/views/browse.html',
                controller: 'BrowseController',
                controllerAs: 'vm',
                resolve: {
                    currentSets: ['browseData', function(browseData) {
                        return browseData.getSets().then(function(sets) {
                            return _.sortBy(sets, 'Name');
                        });
                    }]
                }
            });
    }
})();
