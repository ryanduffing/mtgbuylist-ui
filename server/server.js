var express = require('express'),
    app = express(),
    request = require('request').defaults({jar: true});

var serviceBase = 'http://localhost:51126';

app.get('/api/*', function(req, res) {
    'use strict';
    var url = serviceBase + req.url;
    console.log('[GET]: ' + url);
    var r = request(url);
    req.pipe(r).pipe(res);
});

app.post('/api/*', function(req, res) {
    'use strict';
    var url = serviceBase + req.url;
    console.log('[POST]: ' + url);
    var options = {uri: url};
    if (req.body) {
        options.form = req.body;
    }
    var r = request.post(options);
    req.pipe(r).pipe(res);
    rewriteSetCookie(res);
});

app.put('/api/*', function(req, res) {
    'use strict';
    var url = serviceBase + req.url;
    console.log('[PUT]: ' + url);
    var options = {uri: url};
    if (req.body) {
        options.form = req.body;
    }
    var r = request.put(options);
    req.pipe(r).pipe(res);
});

app.delete('/api/*', function(req, res) {
    'use strict';
    var url = serviceBase + req.url;
    console.log('[DELETE]: ' + url);
    var options = {uri: url};
    if (req.body) {
        options.form = req.body;
    }
    var r = request.del(options);
    req.pipe(r).pipe(res);
});

function rewriteSetCookie(res) {
    var oldWriteHead = res.writeHead;
    res.writeHead = function() {
        var cookies = res.get('set-cookie');

        if(cookies && cookies.length > 0) {
            for(var i = 0; i < cookies.length; i++) {
                var setCookie = cookies[i].replace(/(domain=)[^\s]*;/i, '').replace(/secure(;)?/, '');

                res.setHeader('Set-Cookie', setCookie);
            }

            oldWriteHead.apply(res, arguments);
        }
    };
}

module.exports = app;
